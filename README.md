## 一个简单的表白demo

只需要在页面中修改如下的name属性为你喜欢的人的名字即可。

```js
new girlFriend({
    text:"做我女朋友好不好?",
    textEl:$('#text'),
    name:"XXX",
    nameEl:$('#name'),
    notWellTextArr:["房产证写你名字","难产保大人","我妈会游泳","余生请多指教"],
    notWellEl:$('#not-well'),
    buttonEls:$('.btn-group button'),
    wellTextArr:["太好了，我太开心了","接下来，我要给你一个惊喜","继续下一步"],
    popBox:$('.popBox'),
    isBirth:false, //是否是生日
    poems:["情如风雨无寻常，不动平平动即殇。<br>愿得世上有情人，心心相印似鸳鸯。"] //情诗
});

```

### 本地运行

由于本站是基于AJAX动态加载技术，所以在本地无法直接通过双击“index.html”的方式正确启动（打开之后的页面会没有内容）。要启动网站需将网站放置于本地HTTP服务的环境下（如Apache,IIS），当然也可以通过使用代码编辑器安装“live-server”来进行启动，还可以通过npm全局下载“live-server”之后来到网站根目录下使用命令行输入“live-server”来启动网站。

### 温馨提示

最好在手机端访问，网址为[线上地址](https://eveningwater.gitee.io/lovegirl/)。或者扫描如下二维码访问:

![](./image/code.png)

